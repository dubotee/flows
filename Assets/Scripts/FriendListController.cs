﻿using UnityEngine;
using System.Collections;

public class FriendListController : MonoBehaviour {
	public GameObject friendListPanel;
	// Use this for initialization
	void Start () {
		friendListPanel.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show() 
	{
		friendListPanel.SetActive (true);
	}

	public void FriendClicked()
	{
		GameData.Instance.State = GameData.GameState.ATTACK;
	}
}
