﻿using System;
using System.Collections.Generic;

public class Area
{
    public int AreaId;
    public int Level;
    public List<MicroGame> MicroGames;

    public Area(int areaId)
    {
        AreaId = areaId;
        MicroGames = new List<MicroGame>();

        //Generate 10 microgame for each area, the final will be BOSS
        int initNum = 10 * (areaId - 1) + 1;
        for (int i = initNum; i < initNum + 10; ++i)
        {
            MicroGames.Add(new MicroGame(i, i == initNum + 10 - 1 ? true : false));
        }
    }

    public Area(JSONObject data)
    {
        this.AreaId = (int)data.GetField("AreaId").n;
        this.Level = (int)data.GetField("Level").n;
        this.MicroGames = new List<MicroGame>();
        List<JSONObject> gamesList = data.GetField("MicroGames").list;
        foreach (JSONObject game in gamesList)
        {
            this.MicroGames.Add(new MicroGame(game));
        }
    }

    public JSONObject ToJson()
    {
        JSONObject json = new JSONObject(JSONObject.Type.OBJECT);
        json.AddField("AreaId", AreaId);
        json.AddField("Level", Level);

        JSONObject arrayMicroGame = new JSONObject(JSONObject.Type.ARRAY);
        foreach (MicroGame game in MicroGames)
        {
            arrayMicroGame.Add(game.ToJson());
        }

        json.AddField("MicroGames", arrayMicroGame);

        return json;
    }
}
