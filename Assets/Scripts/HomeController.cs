﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HomeController : MonoBehaviour {
	public Text mStaminaText;
	public Text mAttackText;
	public Text mMoneyText;
	public GameObject building1;
    public GameObject building2;
    public GameObject building3;
    public Button upgradeBuilding;
	public Text buidlingText;
	public Text buildingUpgradeText;
    public Text areaId;
    public GameObject PanelAttacked;

	void Start () 
    {
		GameData.Instance.AddPoint ();
        areaId.text = "Area Id " + GameData.Instance.UserCurrentArea.AreaId.ToString();
	}

	// Update is called once per frame
	void Update () {
		mStaminaText.text = "Stamina: " + GameData.Instance.StaminaPt;
		mAttackText.text =  "Attack: " + GameData.Instance.AttackPt;
		mMoneyText.text =   "Money: " + GameData.Instance.MoneyPt + "$";

        building1.SetActive(GameData.Instance.UserCurrentArea.Level >= 1);
        building2.SetActive(GameData.Instance.UserCurrentArea.Level >= 2);
        building3.SetActive(GameData.Instance.UserCurrentArea.Level >= 3);
        upgradeBuilding.interactable = GameData.Instance.UserCurrentArea.Level < 3;
        
        //building.transform.localScale = new Vector3(Mathf.Clamp(GameData.Instance.UserCurrentArea.Level / 2f * 10, 0f, 20f), GameData.Instance.UserCurrentArea.Level / 4f * 10);
        //building.transform.position = new Vector3 (0f, (building.transform.localScale.y - 30) / 10f);
        
        buidlingText.text = "Building Level " + GameData.Instance.UserCurrentArea.Level;
        buildingUpgradeText.text = "Upgrade\n" + (GameData.Instance.UserCurrentArea.Level * 400) + "$";
	}

	public void PlayClicked()
	{
		if (GameData.Instance.StaminaPt > 0)
			GameData.Instance.State = GameData.GameState.PLAY;
		else
			GetComponent<NoficationController> ().Show ("Not enough Stamina");
	}

	public void AttackClicked() 
	{
		if (GameData.Instance.AttackPt > 0)
			GetComponent<FriendListController> ().Show ();
		else
			GetComponent<NoficationController> ().Show ("Not enough Attack Point");
	}
	
	public void UpgradeClicked() 
	{
        if (GameData.Instance.MoneyPt >= GameData.Instance.UserCurrentArea.Level * 400)
		{
            GameData.Instance.MoneyPt -= GameData.Instance.UserCurrentArea.Level * 400;
			GameData.Instance.UserCurrentArea.Level++;
            if (GameData.Instance.UserCurrentArea.Level == 3)
            {
                GameData.Instance.AvailableAreaList.Add(new Area(2));
            }
		}
		else
			GetComponent<NoficationController> ().Show ("Not enough Money");
	}

    public void BackClicked()
    {
        GameData.Instance.State = GameData.GameState.AREA;
    }

    public void AdjustStatusClicked()
    {
        GameData.Instance.State = GameData.GameState.ADJUST_STATUS;
    }

    public void AttackedClicked()
    {
        if (GameData.Instance.UserCurrentArea.Level > 1)
        {
            GameData.Instance.UserCurrentArea.Level--;
            PanelAttacked.SetActive(true);
        }
    }

    public void AttackedConfirmClicked()
    {
        PanelAttacked.SetActive(false);
    }
}
