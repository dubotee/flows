﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MicroGame {
    public int MicroGameId;
    public List<int> Status;
    public bool IsBoss;
    
    public MicroGame(int microGameId, bool isBoss)
    {
        MicroGameId = microGameId;
        IsBoss = isBoss;
        Status = new List<int>();

        //Generate random number of status for micro game
        int statusCount = GameData.Instance.GetRandMicroGame();
        Debug.Log(statusCount);
        for (int i = 1; i <= statusCount; ++i)
        {
            Status.Add(1);
        }
    }

    public MicroGame(JSONObject data)
    {
        this.MicroGameId = (int)data.GetField("MicroGameId").n;
        this.IsBoss = data.GetField("MicroGameId").b;
        this.Status = new List<int>();
        List<JSONObject> statusList = data.GetField("Status").list;
        foreach (JSONObject status in statusList)
        {
            this.Status.Add((int)status.n);
        }
    }

    public MicroGame(MicroGame previous)
    {
        this.MicroGameId = previous.MicroGameId;
        this.IsBoss = previous.IsBoss;
        this.Status = new List<int>();
        foreach (int i in previous.Status)
        {
            this.Status.Add(i);
        }
    }

    public JSONObject ToJson()
    {
        JSONObject json = new JSONObject(JSONObject.Type.OBJECT);
        json.AddField("MicroGameId", MicroGameId);
        json.AddField("IsBoss", IsBoss);

        JSONObject statusArray = new JSONObject(JSONObject.Type.ARRAY);
        foreach (int i in Status)
        {
            statusArray.Add(i);
        }

        json.AddField("Status", statusArray);

        return json;
    }
}
