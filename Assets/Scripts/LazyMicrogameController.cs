﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LazyMicrogameController : MonoBehaviour {
	public Text mGameTitle;
	public Text mGameResult;
	public Button mPlayButton;

	int currLevel = 0;
	bool isShowingResult = false;
	bool currResult = false;

    List<MicroGame> cloneGameList = new List<MicroGame>(GameData.Instance.UserCurrentArea.MicroGames);
    MicroGame currentGame;
	// Use this for initialization
	void Start () {
		isShowingResult = false;
        SetupGame();
	}
	
	// Update is called once per frame
	void Update () {

        if (currentGame != null)
        {
            mGameTitle.text = currentGame.IsBoss == false ? "Stage " + (currLevel + 1) + "\nMicroGame " + currentGame.MicroGameId : "Boss";
		    mGameResult.text = currResult ? "You Win" : "You Lose";
		    mPlayButton.interactable = !isShowingResult;
		    mGameResult.gameObject.SetActive(isShowingResult);
        }
	}

	public void PlayClicked() {
		isShowingResult = false;
        currResult = Random.Range(0f, 1f) < 0.9f ? true : false;

		isShowingResult = true;

		StartCoroutine(DisplayResult(1.0F));
	}

    public void SetupGame()
    {
        int i = currLevel < 9 ? Random.Range(0, cloneGameList.Count - 2) : 0;
        currentGame = new MicroGame(cloneGameList[i]);
        cloneGameList.Remove(cloneGameList[i]);
    }

	IEnumerator DisplayResult(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		if (currResult)
			currLevel = ++currLevel;
		if(currLevel > 9 || !currResult)
		{
			this.GetComponent<DebriefMicrogameController>().Show(currLevel);
		}
		else
		{
            int i = currLevel < 9 ? Random.Range(0, cloneGameList.Count - 2) : 0;
            currentGame =  new MicroGame(cloneGameList[i]);
            cloneGameList.Remove(cloneGameList[i]);
			isShowingResult = false;
		}
	}
}
