﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MicroGameModController : MonoBehaviour {

    public List<Text> MicroGameTitle;

    Dictionary<int, List<InputField>> InputFieldValue = new Dictionary<int, List<InputField>>();

    Area currentArea;

    public InputField InputFieldPrefab;
    public Text AreaId;
    void Awake()
    {
        currentArea = GameData.Instance.UserCurrentArea;
        AreaId.text = "Area " + currentArea.AreaId;
        for (int i = 0; i < currentArea.MicroGames.Count; ++i)
        {
            MicroGame game = currentArea.MicroGames[i];
            MicroGameTitle[i].text = "Micro Game " + game.MicroGameId;
            List<InputField> inputField = new List<InputField>();
            for (int j = 0; j < game.Status.Count; ++j)
            {
                InputField input = CreateInputField(MicroGameTitle[i].transform, j + 1);
                input.text = game.Status[j].ToString();
                inputField.Add(input);
            }
            InputFieldValue.Add(game.MicroGameId, inputField);
        }
    }

    InputField CreateInputField(Transform parent, int index)
    {
        InputField input = Instantiate<InputField>(InputFieldPrefab);
        input.transform.parent = parent;
        input.transform.localScale = Vector3.one;
        RectTransform rect = input.GetComponent<RectTransform>();
        rect.localPosition = new Vector3(index * 173, 0);
        return input;
    }

    public void BackClicked()
    {
        //Do Save data
        SaveData();
        GameData.Instance.State = GameData.GameState.HOME;
    }

    void SaveData()
    {
        foreach (MicroGame microGame in currentArea.MicroGames)
        {
            List<InputField> inputField = InputFieldValue[microGame.MicroGameId];
            for (int i = 0; i < inputField.Count; ++i)
            {
                microGame.Status[i] = int.Parse(inputField[i].text);
            }
        }
        GameData.Instance.UserCurrentArea = currentArea;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
