﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AreaController : MonoBehaviour {
	public Button area_1;
	public Button area_2;
	public Button area_3;
    public Text unlock_text;

	// Use this for initialization
	void Start () {
        if (GameData.Instance.AvailableAreaList.Count == 1)
        {
            area_1.Select();
            area_3.interactable = false;
        }
        else if (GameData.Instance.AvailableAreaList.Count == 2)
        {
            area_1.Select();
            area_3.Select();
            unlock_text.gameObject.SetActive(true);
        }
	}

    public void Area1Click()
    {
        GameData.Instance.UserCurrentAreaId = 1;
        GoToArea();
    }

    public void Area3Click()
    {
        GameData.Instance.UserCurrentAreaId = 2;
        GoToArea();
    }

    public void ResetDataClicked()
    {
        GameData.Instance.Init();
        area_3.interactable = false;
    }

    void GoToArea()
    {
        GameData.Instance.State = GameData.GameState.HOME;
    }
}
