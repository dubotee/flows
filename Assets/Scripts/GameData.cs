﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : Singleton<GameData> {
	public enum GameState
	{
        AREA,
		HOME,
		PLAY,
		ATTACK,
		SELECT_TARGET,
        ADJUST_STATUS
	}

	GameState _state;

	public int StaminaPt;
	public int AttackPt;
	public int MoneyPt;

	public int AddedStaminaPt;
	public int AddedAttackPt;
	public int AddedMoneyPt;

	public int BuidingLevel;

    private int m_UserCurrentAreaId;

    public int UserCurrentAreaId
    {
        get { return m_UserCurrentAreaId; }
        set { 
            m_UserCurrentAreaId = value;
            UserCurrentArea = AvailableAreaList[m_UserCurrentAreaId - 1];
        }
    }
    public Area UserCurrentArea;
    public List<Area> AvailableAreaList;

    const string USER_DATA = "USER_DATA";

	// Use this for initialization
	void Awake () {
        if (PlayerPrefs.HasKey(USER_DATA) == false)
        {
		    Init ();
        }
        else
        {
            LoadData();
        }
	}

    void OnApplicationQuit()
    {
        SaveData();
    }

    public void Init()
    {
        StaminaPt = 5;
        AttackPt = 5;
        MoneyPt = 1000;
        AvailableAreaList = new List<Area>();
        AvailableAreaList.Add(new Area(1));
        m_UserCurrentAreaId = 1;
        UserCurrentArea = AvailableAreaList[m_UserCurrentAreaId - 1];
    }

    private void SaveData()
    {
        JSONObject json = new JSONObject(JSONObject.Type.OBJECT);
        json.AddField("StaminaPt", this.StaminaPt);
        json.AddField("AttackPt", this.AttackPt);
        json.AddField("MoneyPt", this.MoneyPt);
        json.AddField("UserCurrentAreaId", this.m_UserCurrentAreaId);

        JSONObject areaArray = new JSONObject(JSONObject.Type.ARRAY);
        foreach (Area a in AvailableAreaList)
        {
            areaArray.Add(a.ToJson());
        }
        json.AddField("AvailableAreaList", areaArray);

        PlayerPrefs.SetString(USER_DATA, json.ToString());
        PlayerPrefs.Save();
    }

    private void LoadData()
    {
        JSONObject dataJson = new JSONObject(PlayerPrefs.GetString(USER_DATA));
        this.StaminaPt = (int)dataJson.GetField("StaminaPt").n;
        this.AttackPt = (int)dataJson.GetField("AttackPt").n;
        this.MoneyPt = (int)dataJson.GetField("MoneyPt").n;
        this.m_UserCurrentAreaId = (int)dataJson.GetField("UserCurrentAreaId").n;

        List<JSONObject> areaList = dataJson.GetField("AvailableAreaList").list;
        this.AvailableAreaList = new List<Area>();
        foreach (JSONObject areaData in areaList)
        {
            this.AvailableAreaList.Add(new Area(areaData));
        }

        UserCurrentArea = AvailableAreaList[m_UserCurrentAreaId - 1];
    }

	public void AddPoint()
	{
		StaminaPt += AddedStaminaPt;
		AttackPt += AddedAttackPt;
		MoneyPt += AddedMoneyPt;

		StaminaPt = StaminaPt < 0 ? 0 : StaminaPt;
		AttackPt = AttackPt < 0 ? 0 : AttackPt;
		MoneyPt = MoneyPt < 0 ? 0 : MoneyPt;

		AddedStaminaPt = 0;
		AddedAttackPt = 0;
		AddedMoneyPt = 0;
	}

	public GameState State 
	{
		get {
			return _state;
		}
		set {
			_state = value;
			switch (_state)
			{
			case GameState.HOME:
				if (Application.loadedLevelName != "Home") Application.LoadLevel("Home");
				break;
			case GameState.PLAY:
			case GameState.ATTACK:
				if (Application.loadedLevelName != "MicroGame") Application.LoadLevel("MicroGame");
				break;
            case GameState.ADJUST_STATUS:
            if (Application.loadedLevelName != "MicroGameMod") Application.LoadLevel("MicroGameMod");
            break;
                case GameState.AREA:
            if (Application.loadedLevelName != "Area") Application.LoadLevel("Area");
            break;
			}
		}
	}

    int statusCount;
    public int GetRandMicroGame()
    {
        System.Random rand = new System.Random();
        statusCount = rand.Next(1, 60);
        if (statusCount <= 20)
            return 1;
        else if (statusCount > 20 && statusCount <= 40)
        {
            return 2;
        }
        else
        {
            return 3;
        }
    }
}
