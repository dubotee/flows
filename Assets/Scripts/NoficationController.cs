﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class NoficationController : MonoBehaviour {
	public GameObject notificationPanel;
	public Text notificationText;

	void Start()
	{
		notificationPanel.SetActive (false);
	}

	public void Show(string notificationStr) 
	{
		notificationText.text = notificationStr;
		notificationPanel.SetActive (true);
	}
	
	public void CloseClicked()
	{
		notificationPanel.SetActive (false);
	}
}
