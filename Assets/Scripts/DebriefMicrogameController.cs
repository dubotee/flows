﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebriefMicrogameController : MonoBehaviour {

	public GameObject debriefWindow;
	public Text resultText;

	// Use this for initialization
	void Start () {
		debriefWindow.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void Show(int reachedLevel) 
	{
		bool isWon = reachedLevel > 9;

		if (GameData.Instance.State == GameData.GameState.ATTACK) 
		{
			if (isWon)
			{
				GameData.Instance.AddedStaminaPt++;
				resultText.text = "You got: 1 Stamina\nEnemy building level is reduced to 1";
			}
			else
			{
				GameData.Instance.AddedAttackPt--;
				resultText.text = "You lost: 1 Attack Point";
			}
		}
		else
		{
			if (isWon)
			{
				GameData.Instance.AddedAttackPt++;
				GameData.Instance.AddedMoneyPt = 500;
				resultText.text = "You got: 1 Attack Point\nYou got: 500 Money Point";
			}
			else
			{
				GameData.Instance.AddedStaminaPt--;
				resultText.text = "You lost: 1 Stamina";
			}
		}

		debriefWindow.SetActive (true);
	}

	public void BackClicked()
	{
		GameData.Instance.State = GameData.GameState.HOME;
	}
}
